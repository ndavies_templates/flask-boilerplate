#!/usr/bin/env python

import datetime

from flask import Blueprint, request, render_template
from flask_application import app

frontend = Blueprint('frontend', __name__, template_folder='templates')

@frontend.route('/')
def index():
    if app.debug:
        app.logger.debug('rendering index')
    return render_template(
                'frontend/index.html',
                config=app.config,
                now=datetime.datetime.now,
            )

