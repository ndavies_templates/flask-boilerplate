#!/usr/bin/env python

import os
import logging

from flask import Flask
from flask_application.db import db
from flask_application.helpers import datetimeformat

# Logging
logging.basicConfig(
    level=logging.DEBUG,
    format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
    datefmt='%Y%m%d-%H:%M%p',
)

FLASK_APP_DIR = os.path.dirname(os.path.abspath(__file__))

# Flask
app = Flask(__name__)

# Config
if os.getenv('DEV') == 'yes':
    app.config.from_object('flask_application.config.DevelopmentConfig')
    app.logger.info("Config: Development")
elif os.getenv('TEST') == 'yes':
    app.config.from_object('flask_application.config.TestConfig')
    app.logger.info("Config: Test")
else:
    app.config.from_object('flask_application.config.ProductionConfig')
    app.logger.info("Config: Production")

# Helpers
app.jinja_env.filters['datetimeformat'] = datetimeformat

# Import blueprints
from flask_application.frontend.routes import frontend

# Setup DB
db.init_app(app)
import flask_application.common.models

# Register blueprints
app.register_blueprint(frontend)
