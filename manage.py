from flaskext.script import Manager
from flask_application import app
from flask_application.db import db

manager = Manager(app)

@manager.command
def runserver():
    if app.debug:
        app.run(debug=True)
    else:
        app.run(host='0.0.0.0')

@manager.command
def init_db():
    db.create_all()

@manager.command
def drop_db():
    db.drop_all()

@manager.command
def reload_db():
    db.drop_all()
    db.create_all()

if __name__ == "__main__":
    manager.run()
